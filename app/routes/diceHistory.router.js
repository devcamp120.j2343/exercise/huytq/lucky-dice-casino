const express = require("express");

const diceHistoryMiddleware = require("../middlewares/diceHistory.middlewares");
const diceHistoryController = require("../controllers/diceHistory.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL diceHistory : ", req.url);

    next();
});

router.get("/", diceHistoryMiddleware.getAllDiceHistoryMiddleware, diceHistoryController.getAllDiceHistory)

router.post("/", diceHistoryMiddleware.createDiceHistoryMiddleware, diceHistoryController.createDiceHistory);

router.get("/:diceHistoryId", diceHistoryMiddleware.getDetailDiceHistoryMiddleware, diceHistoryController.getDiceHistoryById);

router.put("/:diceHistoryId", diceHistoryMiddleware.updateDiceHistoryMiddleware, diceHistoryController.updateDiceHistory);

router.delete("/:diceHistoryId", diceHistoryMiddleware.deleteDiceHistoryMiddleware, diceHistoryController.deleteDiceHistory);


module.exports = router;
