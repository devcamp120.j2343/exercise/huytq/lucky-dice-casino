const express = require("express");

const prizeMiddleware = require("../middlewares/prize.middlewares");
const prizeController = require("../controllers/prize.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL prize : ", req.url);

    next();
});

router.get("/", prizeMiddleware.getAllPrizeMiddleware, prizeController.getAllPrize)

router.post("/", prizeMiddleware.createPrizeMiddleware, prizeController.createPrize);

router.get("/:prizeId", prizeMiddleware.getDetailPrizeMiddleware, prizeController.getPrizeById);

router.put("/:prizeId", prizeMiddleware.updatePrizeMiddleware, prizeController.updatePrize);

router.delete("/:prizeId", prizeMiddleware.deletePrizeMiddleware, prizeController.deletePrize);


module.exports = router;
