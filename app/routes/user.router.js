const express = require("express");

const userMiddleware = require("../middlewares/user.middlewares");
const userController = require("../controllers/user.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL user : ", req.url);

    next();
});

router.get("/", userMiddleware.getAllUserMiddleware, userController.getAllUser)

router.post("/", userMiddleware.createUserMiddleware, userController.createUser);

router.get("/:userId", userMiddleware.getDetailUserMiddleware, userController.getUserById);

router.put("/:userId", userMiddleware.updateUserMiddleware, userController.updateUser);

router.delete("/:userId", userMiddleware.deleteUserMiddleware, userController.deleteUser);


module.exports = router;
