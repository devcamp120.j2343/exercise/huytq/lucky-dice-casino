const express = require("express");

const voucherMiddleware = require("../middlewares/voucher.middlewares");
const voucherController = require("../controllers/voucher.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL voucher : ", req.url);

    next();
});

router.get("/", voucherMiddleware.getAllVoucherMiddleware, voucherController.getAllVoucher)

router.post("/", voucherMiddleware.createVoucherMiddleware, voucherController.createVoucher);

router.get("/:voucherId", voucherMiddleware.getDetailVoucherMiddleware, voucherController.getVoucherById);

router.put("/:voucherId", voucherMiddleware.updateVoucherMiddleware, voucherController.updateVoucher);

router.delete("/:voucherId", voucherMiddleware.deleteVoucherMiddleware, voucherController.deleteVoucher);


module.exports = router;
