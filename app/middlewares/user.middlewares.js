const getAllUserMiddleware = (req, res, next) => {
    console.log("Get all User  Middleware");

    next();
}

const createUserMiddleware = (req, res, next) => {
    console.log("Create User  Middleware");

    next();
}

const getDetailUserMiddleware = (req, res, next) => {
    console.log("Get detail User  Middleware");

    next();
}

const updateUserMiddleware = (req, res, next) => {
    console.log("Update User  Middleware");

    next();
}

const deleteUserMiddleware = (req, res, next) => {
    console.log("Delete User  Middleware");

    next();
}

module.exports = {
    getAllUserMiddleware,
    createUserMiddleware,
    getDetailUserMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}

