const getAllDiceHistoryMiddleware = (req, res, next) => {
    console.log("Get all DiceHistory  Middleware");

    next();
}

const createDiceHistoryMiddleware = (req, res, next) => {
    console.log("Create DiceHistory  Middleware");

    next();
}

const getDetailDiceHistoryMiddleware = (req, res, next) => {
    console.log("Get detail DiceHistory  Middleware");

    next();
}

const updateDiceHistoryMiddleware = (req, res, next) => {
    console.log("Update DiceHistory  Middleware");

    next();
}

const deleteDiceHistoryMiddleware = (req, res, next) => {
    console.log("Delete DiceHistory  Middleware");

    next();
}

module.exports = {
    getAllDiceHistoryMiddleware,
    createDiceHistoryMiddleware,
    getDetailDiceHistoryMiddleware,
    updateDiceHistoryMiddleware,
    deleteDiceHistoryMiddleware
}

