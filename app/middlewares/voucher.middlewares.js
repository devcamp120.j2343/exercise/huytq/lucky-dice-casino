const getAllVoucherMiddleware = (req, res, next) => {
    console.log("Get all Voucher  Middleware");

    next();
}

const createVoucherMiddleware = (req, res, next) => {
    console.log("Create Voucher  Middleware");

    next();
}

const getDetailVoucherMiddleware = (req, res, next) => {
    console.log("Get detail Voucher  Middleware");

    next();
}

const updateVoucherMiddleware = (req, res, next) => {
    console.log("Update Voucher  Middleware");

    next();
}

const deleteVoucherMiddleware = (req, res, next) => {
    console.log("Delete Voucher  Middleware");

    next();
}

module.exports = {
    getAllVoucherMiddleware,
    createVoucherMiddleware,
    getDetailVoucherMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
}

