const getAllPrizeMiddleware = (req, res, next) => {
    console.log("Get all Prize  Middleware");

    next();
}

const createPrizeMiddleware = (req, res, next) => {
    console.log("Create Prize  Middleware");

    next();
}

const getDetailPrizeMiddleware = (req, res, next) => {
    console.log("Get detail Prize  Middleware");

    next();
}

const updatePrizeMiddleware = (req, res, next) => {
    console.log("Update Prize  Middleware");

    next();
}

const deletePrizeMiddleware = (req, res, next) => {
    console.log("Delete Prize  Middleware");

    next();
}

module.exports = {
    getAllPrizeMiddleware,
    createPrizeMiddleware,
    getDetailPrizeMiddleware,
    updatePrizeMiddleware,
    deletePrizeMiddleware
}

