//import thư viện mongoose
const mongoose = require('mongoose');

// import user model
const userModel = require('../models/user.model');

//import diceHistory model
const diceHistoryModel = require('../models/diceHistory.model');

// const create user 
const createUser = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        diceHistoryId,
        username,
        firstname,
        lastname,
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "diceHistory ID is not valid"
        })
    }

    if (!username) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "user name is require"
        })
    }

    if (!firstname) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "first name is require"
        })
    }
    if (!lastname) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "last name is require"
        })
    }

    // B3: Thao tác với CSDL
    var newUser = {
        _id: new mongoose.Types.ObjectId(),
        username,
        firstname,
        lastname
    }

    try {
        const createdUser = await userModel.create(newUser);
        
        const updatedDiceHisTory = await diceHistoryModel.findByIdAndUpdate(diceHistoryId, {
            $push: { user: createdUser._id }
        })

        return res.status(201).json({
            status: "Create user successfully",
            diceHistory: updatedDiceHisTory,
            data: createdUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllUser = async (req, res) => {
    //B1: thu thập dữ liệu
    const diceHistoryId = req.query.diceHistoryId;
    //B2: kiểm tra
    if (diceHistoryId !== undefined && !mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "diceHistoryId  is not valid"
        })
    }
    //B3: thực thi model
    try {
        if(diceHistoryId === undefined) {
            const userList = await userModel.find();

            if(userList && userList.length > 0) {
                return res.status(200).json({
                    status: "Get all user sucessfully",
                    data: userList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any user"
                })
            }
        } else {
            const diceHistoryInfo = await diceHistoryModel.findById(diceHistoryId).populate("user");

            return res.status(200).json({
                status: "Get all user of dice history sucessfully",
                data: diceHistoryInfo.user
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getUserById = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const userInfo = await userModel.findById(userId);

        if (userInfo) {
            return res.status(200).json({
                status:"Get user by id sucessfully",
                data: userInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateUser = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    const {username,firstname,lastname} = req.body;

    //B2: kiểm tra dữ liệu
    if (!username) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "user name is require"
        })
    }

    if (!firstname) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "first name is require"
        })
    }
    if (!lastname) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "last name is require"
        })
    }

    //B3: thực thi model
    try {
        let updateUser = {
            username,firstname,lastname
        }

        const updatedUser = await userModel.findByIdAndUpdate(userId, updateUser);

        if (updatedUser) {
            return res.status(200).json({
                status:"Update user sucessfully",
                data: updatedUser
            })
        } else {
            return res.status(404).json({
                status:"Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteUser = async (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;
    var diceHistoryId = req.params.diceHistoryId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedUser = await userModel.findByIdAndDelete(userId);
        // Nếu có diceHistory Id thì xóa thêm (optional)
        if(diceHistoryId !== undefined) {
            await diceHistoryModel.findByIdAndUpdate(diceHistoryId, {
                $pull: { user: userId }
            })
        }
        if (deletedUser) {
            return res.status(200).json({
                status:"Delete user sucessfully",
                data: deletedUser
            })
        } else {
            return res.status(404).json({
                status:"Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllUser,
    createUser,
    getUserById,
    updateUser,
    deleteUser
}

