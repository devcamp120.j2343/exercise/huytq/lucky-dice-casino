//import thư viện mongoose
const mongoose = require('mongoose');

// import dice History model
const diceHistoryModel = require('../models/diceHistory.model');

// const create dice History 
const createDiceHistory = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        dice
    } = req.body;

    // B2: Validate dữ liệu
    
    if (!dice) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "dice is require"
        })
    }
    if (dice < 0 ) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "dice is not valid"
        })
    }
    if ( dice > 6) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "dice is not valid"
        })
    }
    
    // B3: Thao tác với CSDL
    var newDiceHistory = {
        _id: new mongoose.Types.ObjectId(),
        dice,
    }

    try {
        const createdDiceHistory = await diceHistoryModel.create(newDiceHistory);
        return res.status(201).json({
            status: "Create dice history successfully",
            data: createdDiceHistory
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllDiceHistory = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
            const diceHistoryList = await diceHistoryModel.find();

            if(diceHistoryList && diceHistoryList.length > 0) {
                return res.status(200).json({
                    status: "Get all dice history sucessfully",
                    data: diceHistoryList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any dice history"
                })
            }
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getDiceHistoryById = async (req, res) => {
    //B1: thu thập dữ liệu
    var diceHistoryId = req.params.diceHistoryId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const diceHistoryInfo = await diceHistoryModel.findById(diceHistoryId);

        if (diceHistoryInfo) {
            return res.status(200).json({
                status:"Get dice history by id sucessfully",
                data: diceHistoryInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any dice history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateDiceHistory = async (req, res) => {
    //B1: thu thập dữ liệu
    var diceHistoryId = req.params.diceHistoryId;

    const {dice} = req.body;

    //B2: kiểm tra dữ liệu
    if (!dice) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "dice is require"
        })
    }
    if (dice < 0 ) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "dice is not valid"
        })
    }
    if ( dice > 6) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "dice is not valid"
        })
    }

    //B3: thực thi model
    try {
        let updateDiceHistory = {
            dice
        }

        const updatedDiceHistory = await diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory);

        if (updatedDiceHistory) {
            return res.status(200).json({
                status:"Update dice history sucessfully",
                data: updatedDiceHistory
            })
        } else {
            return res.status(404).json({
                status:"Not found any dice history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteDiceHistory = async (req, res) => {
    //B1: Thu thập dữ liệu
    var diceHistoryId = req.params.diceHistoryId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedDiceHistory = await diceHistoryModel.findByIdAndDelete(diceHistoryId);
    
        if (deletedDiceHistory) {
            return res.status(200).json({
                status:"Delete dice history sucessfully",
                data: deletedDiceHistory
            })
        } else {
            return res.status(404).json({
                status:"Not found any dice history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllDiceHistory,
    createDiceHistory,
    getDiceHistoryById,
    updateDiceHistory,
    deleteDiceHistory
}

