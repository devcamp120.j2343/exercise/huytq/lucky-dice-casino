//import thư viện mongoose
const mongoose = require('mongoose');

// import Prize  model
const prizeModel = require('../models/prize.model');

// const create Prize  
const createPrize = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        name,
        description,
    } = req.body;

    // B2: Validate dữ liệu
    
    if (!name) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Name is require"
        })
    }
    
    // B3: Thao tác với CSDL
    var newPrize = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    }

    try {
        const createdPrize = await prizeModel.create(newPrize);
        return res.status(201).json({
            status: "Create Prize  successfully",
            data: createdPrize
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllPrize = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
            const prizeList = await prizeModel.find();

            if(prizeList && prizeList.length > 0) {
                return res.status(200).json({
                    status: "Get all Prize  sucessfully",
                    data: prizeList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any Prize "
                })
            }
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getPrizeById = async (req, res) => {
    //B1: thu thập dữ liệu
    var prizeId = req.params.prizeId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const prizeInfo = await prizeModel.findById(prizeId);

        if (prizeInfo) {
            return res.status(200).json({
                status:"Get Prize  by id sucessfully",
                data: prizeInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any Prize "
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updatePrize = async (req, res) => {
    //B1: thu thập dữ liệu
    var prizeId = req.params.prizeId;

    const {name,description} = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Name is require"
        })
    }

    //B3: thực thi model
    try {
        let updatePrize = {
            name,
            description
        }

        const updatedPrize = await prizeModel.findByIdAndUpdate(prizeId, updatePrize);

        if (updatedPrize) {
            return res.status(200).json({
                status:"Update Prize  sucessfully",
                data: updatedPrize
            })
        } else {
            return res.status(404).json({
                status:"Not found any Prize "
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deletePrize = async (req, res) => {
    //B1: Thu thập dữ liệu
    var prizeId = req.params.prizeId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedPrize = await prizeModel.findByIdAndDelete(prizeId);
    
        if (deletedPrize) {
            return res.status(200).json({
                status:"Delete Prize  sucessfully",
                data: deletedPrize
            })
        } else {
            return res.status(404).json({
                status:"Not found any Prize "
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllPrize,
    createPrize,
    getPrizeById,
    updatePrize,
    deletePrize
}

