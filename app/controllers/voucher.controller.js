//import thư viện mongoose
const mongoose = require('mongoose');

// import voucher model
const voucherModel = require('../models/voucher.model');

// const create voucher 
const createVoucher = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        code,
        discount,
        note,
    } = req.body;

    // B2: Validate dữ liệu
    
    if (!code) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "code is require"
        })
    }
    if (!discount) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Discount is require"
        })
    }
    if ( discount < 0 || discount > 100) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "discount is not valid"
        })
    }
    
    // B3: Thao tác với CSDL
    var newVoucher = {
        _id: new mongoose.Types.ObjectId(),
        code,
        discount,
        note
    }

    try {
        const createdVoucher = await voucherModel.create(newVoucher);
        return res.status(201).json({
            status: "Create voucher successfully",
            data: createdVoucher
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllVoucher = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
            const voucherList = await voucherModel.find();

            if(voucherList && voucherList.length > 0) {
                return res.status(200).json({
                    status: "Get all voucher sucessfully",
                    data: voucherList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher"
                })
            }
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getVoucherById = async (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const voucherInfo = await voucherModel.findById(voucherId);

        if (voucherInfo) {
            return res.status(200).json({
                status:"Get voucher by id sucessfully",
                data: voucherInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any voucher "
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateVoucher = async (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;

    const {code,discount,note} = req.body;

    //B2: kiểm tra dữ liệu
    if (!code) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "code is require"
        })
    }
    if (!discount) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Discount is require"
        })
    }
    if ( discount < 0 || discount > 100) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "discount is not valid"
        })
    }

    //B3: thực thi model
    try {
        let updateVoucher = {
            code,
            discount,
            note
        }

        const updatedVoucher = await voucherModel.findByIdAndUpdate(voucherId, updateVoucher);

        if (updatedVoucher) {
            return res.status(200).json({
                status:"Update voucher sucessfully",
                data: updatedVoucher
            })
        } else {
            return res.status(404).json({
                status:"Not found any voucher history"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteVoucher = async (req, res) => {
    //B1: Thu thập dữ liệu
    var voucherId = req.params.voucherId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedVoucher = await voucherModel.findByIdAndDelete(voucherId);
    
        if (deletedVoucher) {
            return res.status(200).json({
                status:"Delete voucher sucessfully",
                data: deletedVoucher
            })
        } else {
            return res.status(404).json({
                status:"Not found any voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllVoucher,
    createVoucher,
    getVoucherById,
    updateVoucher,
    deleteVoucher
}

