//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const voucherHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId, 
    user: {
        type: mongoose.Types.ObjectId,
        require: true,
        ref:"user"
    },
    voucher: {
        type:mongoose.Types.ObjectId,
        require: true,
        ref:"voucher"
    }
},{timestamps: true});

//b4 biên dịch schema thành model
module.exports = mongoose.model("voucherhistory",voucherHistorySchema)
