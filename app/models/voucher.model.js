//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
     
    code: {
        type: String,
        require: true,
        unique: true,
    },
    discount :{
        type:Number,
        require:true,
    },
    note: {
        type:String,
        require: false
    }
},{timestamps: true});

//b4 biên dịch schema thành model
module.exports = mongoose.model("voucher",voucherSchema)
