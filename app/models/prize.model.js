//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const prizeSchema = new Schema({
    _id: mongoose.Types.ObjectId, 
    name: {
        type: String,
        require: true,
        unique: true
    },
    description: {
        type:String,
        require: false
    }
},{timestamps: true});

//b4 biên dịch schema thành model
module.exports = mongoose.model("prize",prizeSchema)
