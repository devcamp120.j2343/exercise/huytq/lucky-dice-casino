//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    username: {
        type: String,
        require: true,
        unique: true
    },
    firstname: {
        type: String,
        require: true,
         
    },
    lastname: {
        type:String,
        require:true,
    },
    

},{timestamps: true});

//b4 biên dịch schema thành model
module.exports = mongoose.model("user",userSchema)
