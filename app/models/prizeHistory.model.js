//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const prizeHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId, 
    user: {
        type: mongoose.Types.ObjectId,
        require: true,
        ref:"user"
    },
    prize: {
        type:mongoose.Types.ObjectId,
        require: true,
        ref:"prize"
    }
},{timestamps: true});

//b4 biên dịch schema thành model
module.exports = mongoose.model("prizeHistory",prizeHistorySchema)
