//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const diceHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user:
        {
            type: mongoose.Types.ObjectId,
            require: true,
            ref:"user"
        },
     
    dice: {
        type: Number,
        require: true,
         
    },
},{timestamps: true});

//b4 biên dịch schema thành model
module.exports = mongoose.model("dicehistory",diceHistorySchema)
