//Import thư viện ExpressJS
// import express from 'express';
const express = require('express');
const cors = require('cors');
const path = require('path');
//import thư viện mongoose
const mongoose = require('mongoose');

// Khởi tạo app nodejs bằng express
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

//khai bao router
const userRouter = require("./app/routes/user.router");
const diceHistoryRouter = require("./app/routes/diceHistory.router");
const prizeRouter = require("./app/routes/prize.router");
const voucherRouter = require("./app/routes/voucher.router");
// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());
app.use(cors());

//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((request, response, next) => {
    var today = new Date();

    console.log("Current time: ", today);

    next();
});

//Middleware in ra console request method mỗi khi API gọi
app.use((request, response, next) => {
    console.log("Method: ", request.method);

    next();
});

// Khai báo API trả về chuỗi
app.get("/random-number", (request, response) => {
    //Viết code xử lý trong đây
    var max= 6;
    var randomNumber = Math.floor(Math.random() * max);
    var resultString = `Random number:  ${randomNumber}`;

    response.json({
        result: resultString
    });
});

//ket noi database mongo
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDice")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));
  //Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/app/views/"));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/task 17.10.html"));
})
//khai báo các model
const userModel = require('./app/models/user.model');
const diceHistoryModel = require('./app/models/diceHistory.model');
const prizeModel = require('./app/models/prize.model');
const voucherModel = require('./app/models/voucher.model');
const prizeHistoryModel = require('./app/models/prizeHistory.model');
const voucherHistoryModel = require('./app/models/voucherHistory.model');
//khai bao api 
app.use("/api/v1/user", userRouter);
app.use("/api/v1/diceHistory",diceHistoryRouter);
app.use("/api/v1/prize",prizeRouter);
app.use("/api/v1/voucher",voucherRouter);
// Chạy ứng dụng
app.listen(port, () => {
    console.log("Ứng dụng chạy trên cổng: ", port);
})
